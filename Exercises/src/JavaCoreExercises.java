public class JavaCoreExercises {
    public static void main(String args[]) {
        System.out.println("Exercise #1");
        System.out.println("Calculate 45 + 78 / 87");
        float a = 45f + 78f / 87f;
        System.out.println("Result is " + a);

        System.out.println("");
        System.out.println("Exercise #2");
        System.out.println("Assign variable A to int 8 and then change it to String “8”");

        int b = 8;
        String numberToString = Integer.toString(b);

        System.out.println("");
        System.out.println("Exercise #3");
        System.out.println("Assign Variable with your name and print string with replaced “My name is ” ");

        String name = "Mikhail";
        System.out.println("My name is " + name);

        System.out.println("");
        System.out.println("Exercise #4");
        System.out.println("If else. - Assign int variable. If var is odd print “number is Odd”");

        int num = 3;
        if (num % 2 == 0)

        { System.out.println("Number is even"); }
        else

        { System.out.print("Number is odd"); }
    }
}
