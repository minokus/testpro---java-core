import java.util.Scanner;

public class Loops {
    public static void main(String[] args) {
        System.out.println("Loops ex. ");
        System.out.println("Enter the string here:");
        Scanner scanner = new Scanner(System.in);
        String line = scanner.nextLine();

        int y = 0;
        for (int u = 0; u < line.length(); u++){
            if (line.charAt(u) != ' ') {
                ++y;
            }
        }

        if (y % 2 == 0) {
            System.out.println("String is even.");
        }
        else {
            System.out.println("String is not even.");
        }

    }
}
